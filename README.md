# ca-gateway conda recipe

Home: https://github.com/epics-extensions/ca-gateway

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: Channel Access PV Gateway
