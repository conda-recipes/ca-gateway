#!/bin/bash

cat << EOF > configure/RELEASE.local
EPICS_BASE=$EPICS_BASE
EOF

make -j$(getconf _NPROCESSORS_ONLN)

# Move the executable to the conda environment bin directory
mv bin/linux-x86_64/gateway ${PREFIX}/bin
